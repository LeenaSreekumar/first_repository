package com.jpm.selenium.driver_cmd;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class WebDriverCommands {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver =ChromeUtil.getChromeDriver();
		//String url = "https://www.google.com";
		String url = "file:\\D:\\Leena\\04Selenium\\11_SeleniumWebDriver\\src\\main\\java\\Locators.html";
		if(driver !=null)
		{
			//get -->to launch the website
			driver.get(url);
			
			//get the title of the webpage
			
			System.out.println("Title of the page "+driver.getTitle());
			System.out.println("Current URL is "+driver.getCurrentUrl());
			
			WebElement element = driver.findElement(By.id("user"));
			System.out.println("Element is : "+element.getAttribute("id"));
			element.sendKeys("admin");//input the data
			
			driver.findElement(By.name("password")).sendKeys("pass123");
			
			element = driver.findElement(By.linkText("How to use locators?"));
			System.out.println("Element link text" + element.getText());
			Thread.sleep(2000);
			element.click();//it will click the link as the WebElement is holding
			
			
		}
		else
		{
			System.out.println("Driver not loaded!");
		}
        
		Thread.sleep(10000);
		
		driver.close();
	}

}
