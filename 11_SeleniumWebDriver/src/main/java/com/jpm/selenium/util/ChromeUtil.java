package com.jpm.selenium.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeUtil {
	
	public static WebDriver getChromeDriver()
	{
		WebDriver driver = null;
		//Step 1: Driver Class
		String driverClassKey = "webdriver.chrome.driver";
		
		//Step 2: driver path (we have kept driver in driver folder
		String driverPath = ".\\driver\\chromedriver.exe";
		
		//Step 3: Set the System class properties
		System.setProperty(driverClassKey, driverPath);
		
		//Step 4: Set the Chrome options
		ChromeOptions options = new ChromeOptions();
		
		//step5: get the chrome driver instance by passing ChromeOptions
		driver = new ChromeDriver(options);
		
		System.out.println("Trying to load chrome browser");
		if(driver != null)
		{
			System.out.println("Driver loaded!!");
		}
		
		return driver;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("opening the browser");
		WebDriver driver = ChromeUtil.getChromeDriver();
		
		System.out.println("Driver is "+driver);
		
		
		
		//driver.close();
		
		System.out.println("closing the browser");
	}

}
