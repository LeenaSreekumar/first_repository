package com.jpm.selenium.locators;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class TestLocators {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		
		String url = "http://demo.opencart.com";
		driver.get(url);
		Thread.sleep(2000);
		
		WebElement searchBox = driver.findElement(By.name("search"));
		searchBox.sendKeys("Phone");
		Thread.sleep(2000);
		
		driver.findElement(By.className("input-group-btn")).click();
		Thread.sleep(2000);
		WebElement searchBox1 = driver.findElement(By.id("input-search"));
		searchBox1.clear();
		searchBox1.sendKeys("Mac");
		Thread.sleep(2000);
		//by.xpath
		driver.findElement(By.xpath("//*[@id='button-search']")).click();
		Thread.sleep(5000);
		driver.navigate().back();
		Thread.sleep(5000);
		//by.tagname
		List<WebElement> listAnchor = driver.findElements(By.tagName("a"));
		
		System.out.println("List of 'a' tags");
		
		/*for (WebElement temp: listAnchor)
		{
			System.out.println(temp.getText());
		}*/
		//java 8 -lambada
		listAnchor.forEach(temp->System.out.println(temp.getText()));
		
		//by.cssSelector
		Thread.sleep(2000);
		List<WebElement> listCss = driver.findElements(By.cssSelector("span.price-tax"));
		System.out.println("List of Css Seletors ");
		listCss.forEach(action->System.out.println(action.getText()));
		Thread.sleep(2000);
		
		
		driver.close();
		
		

	}

}
