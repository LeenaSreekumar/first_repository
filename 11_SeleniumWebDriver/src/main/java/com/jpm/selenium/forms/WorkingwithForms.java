package com.jpm.selenium.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jpm.selenium.util.ChromeUtil;


public class WorkingwithForms {

	public static void main(String[] args) throws InterruptedException {

         WebDriver driver = ChromeUtil.getChromeDriver();
         
         String url = "file:\\D:\\Leena\\04Selenium\\11_SeleniumWebDriver\\src\\main\\java\\WorkingWithForms.html";
         driver.get(url);
         driver.manage().window().maximize();
         
         driver.findElement(By.name("txtUName")).sendKeys("Leena");
         Thread.sleep(2000);
         driver.findElement(By.id("txtPassword")).sendKeys("pass123");
         Thread.sleep(2000);
         driver.findElement(By.id("txtConfPassword")).sendKeys("pass123");
         Thread.sleep(2000);
         
         driver.findElement(By.cssSelector("input.format1")).sendKeys("Leena");
         Thread.sleep(2000);
         driver.findElement(By.cssSelector("input#txtLastName")).sendKeys("Sree");
         
         Thread.sleep(2000);
         List<WebElement> radioElements = driver.findElements(By.name("gender"));
         
         for(WebElement radio : radioElements)
         {
        	 String radioSelected;
        	 radioSelected = radio.getAttribute("value").toString();
        	 
        	 if(radioSelected.equals("Female") )
        	 {
        		 radio.click();
        	 }
        	 
        	
         }         
         Thread.sleep(2000);
         driver.findElement(By.cssSelector("input[type=date]")).sendKeys("12/12/2001");
         Thread.sleep(2000);
         driver.findElement(By.name("Email")).sendKeys("leena@test.com");
         Thread.sleep(2000);
         driver.findElement(By.name("Address")).sendKeys("Hyderabad");
         Thread.sleep(2000);
             
         
         Select drpCity = new Select(driver.findElement(By.name("City")));
         
         drpCity.selectByIndex(2);
         drpCity.selectByValue("Mumbai");
         drpCity.selectByVisibleText("Chennai");
         Thread.sleep(2000);
         
         driver.findElement(By.name("Phone")).sendKeys("983434233");
         Thread.sleep(2000);
         List<WebElement> checkHobbies = driver.findElements(By.name("chkHobbies"));
         for(WebElement hobbyCheckBox : checkHobbies)
         {
        	 String selection = hobbyCheckBox.getAttribute("value").toString();
        	 
        	 if(!selection.equals("Music"))
        	 {
        		 hobbyCheckBox.click();
        	 }
        	 
        	 
         }
         Thread.sleep(5000);
         driver.close();
         

	}

}

