package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpWindowDemo {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		
		String url = "file:\\D:\\Leena\\04Selenium\\11_SeleniumWebDriver\\src\\main\\java\\PopUpWinDemo.html";
		driver.get(url);
		
		Thread.sleep(2000);
		String parentWindow = driver.getWindowHandle();
		System.out.println("Parent Window: "+parentWindow);
		driver.findElement(By.id("newtab")).click();
		
		Thread.sleep(2000);
		parentWindow = driver.getWindowHandle();//parent window
		System.out.println("Parent Window: "+parentWindow);
		Thread.sleep(2000);
		
		Set<String> childWinList = driver.getWindowHandles(); //all the window based
		String child="";
		for(String childWin : childWinList)
		{
			if( !childWin.equals(parentWindow))
			{
				child = childWin;
				Thread.sleep(2000);
				driver.switchTo().window(childWin);
				System.out.println("Child Window "+childWin);
				driver.findElement(By.id("alert")).click();
				Thread.sleep(2000);
				Alert alert = driver.switchTo().alert();
				Thread.sleep(2000);
				alert.accept();//use dismiss() methos to cancel
				
				driver.findElement(By.id("confirm")).click();
				Thread.sleep(2000);				
				Alert alert1 = driver.switchTo().alert();
				Thread.sleep(2000);
				alert.accept();
				
			}
		}
		driver.switchTo().window(parentWindow);
		System.out.println("\n switch to parent window "+parentWindow);
		Thread.sleep(2000);
		driver.findElement(By.id("newwindow")).click();
		Thread.sleep(2000);
		for(String childWin2 : driver.getWindowHandles())
		{
			if( !childWin2.equals(parentWindow) && !childWin2.equals(child))
			{
				driver.switchTo().window(childWin2);
				
				System.out.println("Child Window 2: "+childWin2);
				Thread.sleep(2000);
				//break;
			}
			}
		
		Thread.sleep(5000);
		
		
		driver.quit();

	}

}
